import {AppRoutingModule} from './app-routing.modules';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppComponent} from './app.component';
import {CardsComponent} from './cards/cards.component';
import {CardComponent} from './cards/card/card.component';
import {CardformComponent} from './cards/cardform/cardform.component';
import {DefaultimagePipe} from './pipe/defaultimage.pipe';
import {MembersComponent} from './component/members/members.component';
import {ProductComponent} from './component/product/product.component';
import {Members2Component} from './component/members2/members2.component';
import {ColorDirective} from './directive/color.directive';
import {MyDataService} from './services/my-data.service';
import {CapitalizePipe} from './pipe/capitalize.pipe';
import {UsertemplateComponent} from './component/user/usertemplate/usertemplate.component';
import {CustomermoduleComponent} from './component/customer/customermodule/customermodule.component';
import {PageNotFoundComponent} from './page-not-found.component';
import {MyDataService2} from './services/my-data.service2';
import {HttpClientModule} from '@angular/common/http';
import {DetalleProductoComponent} from './component/product/detalle-producto/detalle-producto.component';
import {ListaArtistasComponent} from './component/artistas/lista-artistas/lista-artistas.component';
import {DetalleArtistaComponent} from './component/artistas/detalle-artista/detalle-artista.component';
import {ArtistaComponent} from './component/artistas/artista.component';
import {CountryModule} from './modules/country/country.module';
import { PersonModule } from './modules/person/person.module';
import { ObservablesComponent } from './component/observables/observables.component';
import { ViewChildAndViewChildrenComponent } from './component/view-child-and-view-children/view-child-and-view-children.component';
import { SystemMessageComponent } from './component/system-message/system-message.component';

@NgModule({
  declarations: [
    AppComponent,
    CardsComponent,
    CardComponent,
    CardformComponent,
    DefaultimagePipe,
    MembersComponent,
    ProductComponent,
    Members2Component,
    ColorDirective,
    CapitalizePipe,
    UsertemplateComponent,
    CustomermoduleComponent,
    PageNotFoundComponent,
    DetalleProductoComponent,
    ListaArtistasComponent,
    DetalleArtistaComponent,
    ArtistaComponent,
    ObservablesComponent,
    ViewChildAndViewChildrenComponent,
    SystemMessageComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    CountryModule,
    PersonModule,
    AppRoutingModule,

  ],
  providers: [MyDataService, MyDataService2],
  bootstrap: [AppComponent]
})
export class AppModule {}
