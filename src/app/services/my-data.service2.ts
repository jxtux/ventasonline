import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

import {Member} from '../domains/member';

@Injectable()
export class MyDataService2 {

  private header = new Headers({'content-type': 'application/json'});

  constructor(protected httpClient: HttpClient) {}

  datosGet(): Observable<Member[]> {
    return this.httpClient.get<Member[]>('http://localhost/members.json');
  }

  public get(name: string, sueldo: number): Observable<Member> {
    const httpParams = new HttpParams().set('name', name).set('sueldo', sueldo.toString());

    return this.httpClient.get<Member>('http://localhost/data1.php', {params: httpParams});
  }

  _errorHandler(error: any) {
    return Observable.throw(error._body);
  }

}


