import { Member } from '../domains/member';
import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class MyDataService {

  obj = {id: '1', name: 'antonio', sueldo: 3000};

  private header = new Headers({'content-type': 'application/json'});

  constructor(private http: Http) {}

  datosGet(): Observable<Member[]> {
    return this.http.get('http://localhost/members.json')
      .map((response) => response.json());
  }

  _errorHandler(error: any) {
     return Observable.throw(error._body);
  }

}


