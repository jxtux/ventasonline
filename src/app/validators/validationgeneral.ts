import {AbstractControl, ValidatorFn, FormControl} from '@angular/forms';

interface IUsernameEmailValidator {
}

export class ValidationGeneral {

  constructor() {}

  static emailDomainValidator(control: FormControl) {
    let email = control.value;
    if (email && email.indexOf("@") != -1) {
      let [_, domain] = email.split("@");
      if (domain !== "codecraft.tv") {
        return {emailValidation: {valid: false}}
      }
    }
    return null;
  }

  static hasExclamationMark(input: FormControl) {
    const hasExclamation = input.value.indexOf('!') >= 0;
    return hasExclamation ? null : {exclamationValidation: {valid: false}};
  }

  static caracterValidacion(punctuation: string) {
    return function(input: FormControl) {
      return input.value.indexOf(punctuation) >= 0 ? null : {caracterValidation: {valid: false}};
    };
  }

  static textValidator(control) {
    if (control.value.length <= 3) {
      return {textValidation: {valid: false}};
    }

    return null;
  }
}