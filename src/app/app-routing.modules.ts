import {AppComponent} from './app.component';
import {ArtistaComponent} from './component/artistas/artista.component';
import {CustomermoduleComponent} from './component/customer/customermodule/customermodule.component';
import {MembersComponent} from './component/members/members.component';
import {Members2Component} from './component/members2/members2.component';
import { ObservablesComponent } from './component/observables/observables.component';
import {DetalleProductoComponent} from './component/product/detalle-producto/detalle-producto.component';
import {ProductComponent} from './component/product/product.component';
import {UsertemplateComponent} from './component/user/usertemplate/usertemplate.component';
import { ViewChildAndViewChildrenComponent } from './component/view-child-and-view-children/view-child-and-view-children.component';
import {PageNotFoundComponent} from './page-not-found.component';
import {ModuleWithProviders, NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
  {
    path: 'country',
    loadChildren: 'app/modules/country/country.module#CountryModule',
    data: {preload: true}
  },
  {
    path: 'person',
    loadChildren: 'app/modules/person/person.module#PersonModule'
  },
  {path: 'artistas', component: ArtistaComponent},
  {path: 'members', component: MembersComponent},
  {
    path: 'product', component: ProductComponent,
    children: [
      {path: 'detalleproduct/:id', component: DetalleProductoComponent},
      {path: 'detalleproduct/:id/:titulo', component: DetalleProductoComponent},
    ]
  },
  {path: 'usertemplate', component: UsertemplateComponent},
  {path: 'members2', component: Members2Component},
  {path: 'customer', component: CustomermoduleComponent},
  {path: 'observable', component: ObservablesComponent},
  {path: 'viewChildren', component: ViewChildAndViewChildrenComponent},
  {path: 'memberRedirect', redirectTo: 'members2', pathMatch: 'full'},
  {path: '',  component: ArtistaComponent},
  {path: '**', component: PageNotFoundComponent},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
