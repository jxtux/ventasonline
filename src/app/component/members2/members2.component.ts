import {Member} from '../../domains/member';
import {MyDataService} from '../../services/my-data.service';
import {MyDataService2} from '../../services/my-data.service2';
import {ValidationGeneral} from '../../validators/validationgeneral';
import {Component, OnInit, OnDestroy} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {ISubscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-members2',
  templateUrl: './members2.component.html',
  styleUrls: ['./members2.component.css']
})
export class Members2Component implements OnInit, OnDestroy {
  formMember: FormGroup;
  members: Member[];
  member: Member;

  errorMsg: any;
  color: String = 'yellow';
  private subscription: Subscription = new Subscription();

  name: String = 'jose antonio jxtux5x';

  imageUrl: String = '';

  constructor(private router: Router, private newService: MyDataService, private newService2: MyDataService2) {
  }

  ngOnInit() {
    this.getDataObtenida2();
    this.getMember();

    this.formMember = new FormGroup({
      firstname: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(10),
        Validators.pattern('[\\w\\-\\s\\/]+')
      ])),
      lastname: new FormControl('value5x', Validators.compose([
        ValidationGeneral.textValidator,
        ValidationGeneral.hasExclamationMark,
        ValidationGeneral.caracterValidacion('@')
      ])),
      language: new FormControl('language'),
    });

    this.formMember.get('firstname').setValue('antonio5x');
  }

  getDataObtenida(): void {
    const subscription = this.newService.datosGet().subscribe(
      (data) => this.members = data,
      (err) => this.errorMsg = err);

    //    suscripciones multiples en una sola suscripcion para no crear varias y cuando se destruyen se destruyen todas
    this.subscription.add(subscription);
  }

  getDataObtenida2(): void {
    const subscription = this.newService2.datosGet().subscribe(
      (data) => this.members = data,
      (err) => this.errorMsg = err);

    this.subscription.add(subscription);
  }

  getMember(): void {
    const subscription = this.newService2.get('antonio', 5000).subscribe(
      (data) => this.member = data,
      (err) => this.errorMsg = err);

    this.subscription.add(subscription);
  }

  enviarData(user) {
    console.log(JSON.stringify(user));
    //    this.formMember.reset();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
