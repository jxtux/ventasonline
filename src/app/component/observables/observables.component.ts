import { Doctor } from '../../domains/doctor';
import {Component, OnInit} from '@angular/core';
import { Http } from '@angular/http';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-observables',
  templateUrl: './observables.component.html',
  styles: []
})
export class ObservablesComponent implements OnInit {

  private data: Observable<number>;
  private values: Array<number> = [];
  private anyErrors: boolean;
  private finished: boolean;
  private status: string;

  private doctors = [];
  
  http: Http
  
  constructor(http: Http) {
    this.http = http;  
  }

  ngOnInit() {
  }

  initObservableSubscribe(): void {
    this.data = new Observable(observer => {
      setTimeout(() => {observer.next(42);}, 1000);
      setTimeout(() => {observer.next(43);}, 2000);
      setTimeout(() => {observer.complete();}, 3000);

      this.status = "Started";
    });

    let subscription = this.data.subscribe(
      value => this.values.push(value),
      error => this.anyErrors = true,
      () => this.finished = true
    );

  }

  initObservableSuscripbeForEach(): void {
    this.data = new Observable(observer => {
      setTimeout(() => {observer.next(42);}, 1000);
      setTimeout(() => {observer.next(43);}, 2000);
      setTimeout(() => {observer.complete();}, 3000);

      this.status = "Started";
    });


    let forEachSubscription = this.data
      .forEach(v => this.values.push(v))
      .then(() => this.status = "Ended");
  }

  initObservableError(): void {
    this.data = new Observable(observer => {
      setTimeout(() => {observer.next(42);}, 1000);
      setTimeout(() => {observer.error(new Error('Something bad happened!'));}, 2000);
      setTimeout(() => {observer.next(50);}, 3000);

      this.status = "Started";
    });

    let subscription = this.data.subscribe(
      value => this.values.push(value),
      error => this.anyErrors = error,
      () => this.finished = true
    );
  }
  
  PersonObservable(): void {
   this.http.get('http://jsonplaceholder.typicode.com/users/')
        .flatMap((response) => response.json())
//        .filter((person) => person.id > 5)
//        .map((person) => "Dr. " + person.name)
        .subscribe((data) => {
          this.doctors.push(data);
        });
  }
}
