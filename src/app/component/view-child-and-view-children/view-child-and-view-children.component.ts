import {SystemMessageComponent} from '../system-message/system-message.component';
import {Component, OnInit, ViewChild, ElementRef, AfterViewInit, Renderer, ViewChildren, QueryList} from '@angular/core';

@Component({
  selector: 'app-view-child-and-view-children',
  templateUrl: './view-child-and-view-children.component.html',
  styles: []
})
export class ViewChildAndViewChildrenComponent implements OnInit, AfterViewInit {
  //Usando el tipo de componente,agarra el primer componente de este tipo
  @ViewChild(SystemMessageComponent) message0: SystemMessageComponent;

  //Usando template variable
  @ViewChild('firstMessage') message1: SystemMessageComponent;
  @ViewChild('secondMessage') message2: SystemMessageComponent;

  @ViewChild('header') header: ElementRef;

  @ViewChildren(SystemMessageComponent) query: QueryList<SystemMessageComponent>;

  constructor(public renderer: Renderer) {}

  ngOnInit() {
    //No se puede utilizar el componente hijo
  }

  ngAfterViewInit() {
    // Ahora puedes utilizar el componente hijo
    this.message0.message = 'Hello World!';
    this.message1.message = 'Hello World 1!';
    this.message2.message = 'Hello World 2!';

    this.query.forEach(
      (item: SystemMessageComponent) => console.log(item.message));
  }

  toggleMessage() {
    this.message0.active ? this.message0.hide() : this.message0.show();
    this.message1.active ? this.message1.hide() : this.message1.show();
    this.message2.active ? this.message2.hide() : this.message2.show();
    this.renderer.setElementStyle(this.header.nativeElement, 'backgroundColor', 'lime');
  }

}

