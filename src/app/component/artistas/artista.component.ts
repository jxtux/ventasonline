import {Artista} from '../../domains/artista';
import {ListaArtistasComponent} from './lista-artistas/lista-artistas.component';
import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styleUrls: ['./artista.component.css'],
})
export class ArtistaComponent implements OnInit {
  public la: Array<Artista>;

  public artista = new Artista();

  constructor() {}

  ngOnInit() {
    this.cargarData();
  }

  cargarData() {
    this.la = [
      {'id': 1, 'name': 'Look What You Made Me Do', 'categoria': 'Electronica', 'imagen': 'http://localhost/images/01.jpg'},
      {'id': 2, 'name': 'More Than You Know', 'categoria': 'Electronica', 'imagen': 'http://localhost/images/02.jpg'},
      {'id': 3, 'name': 'Young Dumb & Broke', 'categoria': 'Electronica', 'imagen': 'http://localhost/images/03.jpg'},
      {'id': 4, 'name': 'Bum Bum Tam Tam', 'categoria': 'Rock', 'imagen': 'http://localhost/images/04.jpg'},
      {'id': 5, 'name': 'Wild Thoughts', 'categoria': 'Rock', 'imagen': 'http://localhost/images/05.jpg'},
    ];
  }

  editArtista(data: Artista): void {
    this.artista = data;
  }
}
