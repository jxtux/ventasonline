import {Artista} from '../../../domains/artista';
import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-lista-artistas',
  templateUrl: './lista-artistas.component.html',
  styleUrls: ['./lista-artistas.component.css']
})
export class ListaArtistasComponent implements OnInit {
  @Input() public artistas: Array<Artista>;

  @Output() artista = new EventEmitter();

  constructor() {}

  ngOnInit() {
  }

  seleccionar(data: Artista) {
    this.artista.emit(data);
  }

}
