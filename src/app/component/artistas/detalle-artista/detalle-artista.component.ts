import {Artista} from '../../../domains/artista';
import {Component, OnInit, Input, OnChanges} from '@angular/core';

@Component({
  selector: 'app-detalle-artista',
  templateUrl: './detalle-artista.component.html',
  styleUrls: ['./detalle-artista.component.css']
})
export class DetalleArtistaComponent implements OnInit, OnChanges {
  @Input() public editArtista: Artista;

  constructor() {}

  ngOnInit() {
  }

  ngOnChanges() {
  }

}
