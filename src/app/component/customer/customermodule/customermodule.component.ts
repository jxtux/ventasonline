import {Customer} from '../../../domains/customer';
import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormControl} from '@angular/forms';

@Component({
  selector: 'app-customermodule',
  templateUrl: './customermodule.component.html',
  styleUrls: ['./customermodule.component.css']
})
export class CustomermoduleComponent implements OnInit {
  public myForm: FormGroup;


  public PAYMENT_METHOD_TYPE = {
    BANK: 'bank',
    CARD: 'card'
  };

  constructor(private _fb: FormBuilder) {}

  ngOnInit() {
    this.myForm = new FormGroup({
      name: new FormControl('Jane Doe'),
      paymentMethod: this.initPaymentMethodFormGroup(),
    });

   // this.subscribePaymentTypeChanges();

    this.setPaymentMethodType(this.PAYMENT_METHOD_TYPE.BANK);
  }

  initPaymentMethodFormGroup() {
    const group = new FormGroup({
      tipo: new FormControl(''),
      card: new FormGroup(this.initPaymentMethodCardModel()),
      bank: new FormGroup(this.initPaymentMethodBankModel()),
    });

    return group;
  }

  initPaymentMethodBankModel() {
    const model = {
      accountNo: new FormControl('', Validators.compose([Validators.required])),
      accountHolder: new FormControl('', Validators.compose([Validators.required])),
      routingNo: new FormControl('', Validators.compose([Validators.required]))
    };

    return model;
  }

  initPaymentMethodCardModel() {
    const cardNoRegex = '^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$';
    const expiryRegex = '^(0[1-9]|1[0-2])\/?([0-9]{4}|[0-9]{2})$';


    // initialize card model
    const model = {
      cardNo: new FormControl('', Validators.compose([Validators.required, Validators.pattern(cardNoRegex)])),
      cardHolder: new FormControl('', Validators.compose([Validators.required])),
      expiry: new FormControl('', Validators.compose([Validators.required, Validators.pattern(expiryRegex)]))
    };

    return model;
  }

  setPaymentMethodType(type: string) {
    this.myForm.get('paymentMethod.tipo').setValue(type);
  }

  subscribePaymentTypeChanges() {

    // controls
    const pmCtrl = (<any>this.myForm).controls.paymentMethod;
    const bankCtrl = pmCtrl.controls.bank;
    const cardCtrl = pmCtrl.controls.card;

    // initialize value changes stream
    const changes$ = pmCtrl.controls.tipo.valueChanges;

    // subscribe to the stream
    changes$.subscribe(paymentMethodType => {
      if (paymentMethodType === this.PAYMENT_METHOD_TYPE.BANK) {
        Object.keys(bankCtrl.controls).forEach(key => {
          bankCtrl.controls[key].setValidators(this.initPaymentMethodBankModel()[key][1]);
          bankCtrl.controls[key].updateValueAndValidity();
        });

        Object.keys(cardCtrl.controls).forEach(key => {
          cardCtrl.controls[key].setValidators(null);
          cardCtrl.controls[key].updateValueAndValidity();
        });
      }

      if (paymentMethodType === this.PAYMENT_METHOD_TYPE.CARD) {
        Object.keys(bankCtrl.controls).forEach(key => {
          bankCtrl.controls[key].setValidators(null);
          bankCtrl.controls[key].updateValueAndValidity();
        });

        Object.keys(cardCtrl.controls).forEach(key => {
          cardCtrl.controls[key].setValidators(this.initPaymentMethodCardModel()[key][1]);
          cardCtrl.controls[key].updateValueAndValidity();
        });
      }

    });
  }

  save(model: Customer, isValid: boolean) {

    console.log(model, isValid);
  }

}
