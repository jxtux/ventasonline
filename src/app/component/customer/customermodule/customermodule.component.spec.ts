import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomermoduleComponent } from './customermodule.component';

describe('CustomermoduleComponent', () => {
  let component: CustomermoduleComponent;
  let fixture: ComponentFixture<CustomermoduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomermoduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomermoduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
