import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-system-message',
  templateUrl: './system-message.component.html',
  styles: []
})
export class SystemMessageComponent implements OnInit {
  @Input() message: string;
  @Input() active: boolean;

  constructor() {}

  ngOnInit() {
  }

  show() {
    this.active = true;
  }
  hide() {
    this.active = false;
  }

}
