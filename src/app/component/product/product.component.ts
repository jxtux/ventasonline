import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  imageUrl: String = '';

  public cursos: Array<any>;

  constructor(private router: Router) {
    this.cursos = [
      {'id': 1, 'titulo': 'Curso de Symfony3'},
      {'id': 2, 'titulo': 'Curso de Zend Framework 2'},
      {'id': 3, 'titulo': 'Aprende PHP desde cero con 36 ejercicios practicos'},
      {'id': 4, 'titulo': 'Curso de Angular 2'}
    ];

  }

  ngOnInit() {
  }

  redireccionar() {
    this.router.navigate(['members2']);
  }

  redirigir() {
    const curso = this.cursos[0];
    this.router.navigate(['artistas']);
  }


}
