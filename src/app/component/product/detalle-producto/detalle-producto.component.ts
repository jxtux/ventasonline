import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-detalle-producto',
  templateUrl: './detalle-producto.component.html',
  styleUrls: ['./detalle-producto.component.css']
})
export class DetalleProductoComponent implements OnInit {
  public idCurso: number;

  public tituloCurso: string;

  constructor(private route: ActivatedRoute , private router: Router) {}

  ngOnInit() {
    this.captarDataHttp();
  }

  captarDataHttp() {
    this.route.params.subscribe(params => {
      if (params['id'] != null) {
        this.idCurso = +params['id'];
      }
      if (params['titulo'] != null) {
        this.tituloCurso = params['titulo'];
      }
    });
  }

}
