export class Card {
  setup: string;
  punchline: string;
  hide: boolean;

  constructor(setup: string, punchline: string, hide: boolean) {
    this.setup = setup;
    this.punchline = punchline;
    this.hide = hide;
  }

  toggle(card) {
    card.hide = !card.hide;
  }
}
