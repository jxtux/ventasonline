export class Person {
  public personId: number;
  public name: string;
  public city: string;

  constructor(personId: number, name: string, city: string) {
    this.personId = personId;
    this.name = name;
    this.city = city;
  }
}
