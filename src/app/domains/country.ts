export class Country {
  public countryId: number;
  public countryName: string;
  public capital: string;
  public currency: string;

  constructor(countryId: number, countryName: string, capital: string, currency: string) {
    this.countryId = countryId;
    this.countryName = countryName;
    this.capital = capital;
    this.currency = currency;
  }

}
