export class Artista {
  public id: number;
  public name: String = '';
  public categoria: String = '';
  public imagen: String = '';
}
