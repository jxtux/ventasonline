import {Theme} from './theme';

export interface Doctor {
  id: number,
  name: string;
  username: string;
  email: string;
  phone: string;
  website: string;
}
