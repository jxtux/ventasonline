import {Card} from '../../domains/card';
import {Component, OnInit, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-cardform',
  templateUrl: './cardform.component.html',
  styleUrls: ['./cardform.component.css']
})
export class CardformComponent implements OnInit {

  @Output() cardCreated = new EventEmitter<Card>();

  createCard(setup: string, punchline: string) {
    this.cardCreated.emit(new Card(setup, punchline, false));
  }

  constructor() {}

  ngOnInit() {
  }

}
