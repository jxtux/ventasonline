import {Component, OnInit} from '@angular/core';

import {Card} from '../domains/card';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {

  cards: Card[];

  card: Card;

  constructor() {
    this.cards = [
      new Card('What did the cheese say when it looked in the mirror?', 'Hello-me (Halloumi)', false),
      new Card('What kind of cheese do you use to disguise a small horse?', 'Mask-a-pony (Mascarpone)', false),
      new Card('A kid threw a lump of cheddar at me', 'I thought �That�s not very mature�', true),
    ];
  }

  ngOnInit() {
  }

  addCard(card) {
    this.cards.unshift(card);
  }
}
