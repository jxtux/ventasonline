import {OnInit, Directive, ElementRef, Renderer, Input, HostListener, HostBinding} from '@angular/core';

@Directive({
  selector: '[appResaltar]'
})
class ResaltarDirective {
  @HostBinding('class.card-outline-primary') private ishovering: boolean;

  @Input() config: Object = {querySelector: '.card-text'};

  constructor(private el: ElementRef, private renderer: Renderer) {
  }
//
//  @HostListener('mouseover') onMouseOver() {
//    const part = this.el.nativeElement.querySelector(this.config.querySelector);
//    this.renderer.setElementStyle(part, 'display', 'block');
//    this.ishovering = true;
//  }
//
//  @HostListener('mouseout') onMouseOut() {
//    const part = this.el.nativeElement.querySelector(this.config.querySelector);
//    this.renderer.setElementStyle(part, 'display', 'none');
//    this.ishovering = false;
//  }

}
