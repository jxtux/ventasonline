import {OnInit, Directive, ElementRef, Renderer, Input, HostListener, HostBinding} from '@angular/core';

@Directive({
  selector: '[appColor]'
})
export class ColorDirective {
  @Input('appColor') appColor: string;
  @Input() tcolor: string;
  @Input() tsize: string;

  constructor(private elRef: ElementRef) {
  }

  @HostListener('mouseover') onMouseOver() {
    this.tcolor = this.tcolor || 'green';
    this.appColor = this.appColor || 'red';
    this.tsize = this.tsize || '20px';
    this.rootColor(this.appColor, this.tcolor, this.tsize);
  }

  @HostListener('mouseout') onMouseOut() {
    this.rootColor(null, null, null);
  }

  private rootColor(color: string, background: string, tsize: string) {
    this.elRef.nativeElement.style.color = color;
    this.elRef.nativeElement.style.backgroundColor = background;
    this.elRef.nativeElement.style.fontSize = tsize;
  }

}
