import { Country } from '../../../domains/country';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class CountryService {

  constructor(protected httpClient: HttpClient) {}

  public get(): Observable<Country[]> {
    return this.httpClient.get<Country[]>('http://localhost/data/country.json');
  }

  getCountry(id: number): Observable<Country> {
    const httpParams = new HttpParams().set('countryId', id.toString());

    return this.httpClient.get<Country>('http://localhost/data/getCountry.php', {params: httpParams});
  }
}
