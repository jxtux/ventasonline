import { Country } from '../../../domains/country';
import { CountryService } from '../service/country.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-list-country',
  templateUrl: './list-country.component.html',
})
export class ListCountryComponent implements OnInit {
   private subscription: Subscription = new Subscription();
   
   countries: Country[];

   errorMsg: any;
  
   constructor(
    private countryService: CountryService,
    private route: ActivatedRoute,
    private router: Router) {}

  ngOnInit() {
    this.getCountry();
  }

  getCountry() {

    const subscription = this. countryService.get().subscribe(
      (data) => this.countries = data,
      (err) => this.errorMsg = err);

    this.subscription.add(subscription);
  }
}
