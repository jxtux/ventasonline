import { Country } from '../../../../domains/country';
import { CountryService } from '../../service/country.service';
import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router, Params} from '@angular/router';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-detail-country',
  templateUrl: './detail-country.component.html',
  styles: []
})
export class DetailCountryComponent implements OnInit {

    country: Country;
  
    constructor(
    private countryService: CountryService,
    private route: ActivatedRoute,
    private router: Router) {}

  ngOnInit() {
    this.detalleCountry();
  }

  detalleCountry() {
    this.route.params
      .switchMap((params: Params) => this.countryService.getCountry(+params['country-id']))
      .subscribe(data => this.country = data );
  }
}
