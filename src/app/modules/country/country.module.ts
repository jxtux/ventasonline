import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CountryRoutingModule} from './country-routing.module';
import {AddCountryComponent} from './add-country/add-country.component';
import {ListCountryComponent} from './list-country/list-country.component';
import {EditCountryComponent} from './list-country/edit-country/edit-country.component';
import {DetailCountryComponent} from './list-country/detail-country/detail-country.component';
import {CountryComponent} from './country.component';
import {CountryService} from './service/country.service';
import { FormsModule, ReactiveFormsModule, } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    CountryRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [AddCountryComponent, ListCountryComponent, EditCountryComponent, DetailCountryComponent, CountryComponent],
  providers: [CountryService]
})
export class CountryModule {}
