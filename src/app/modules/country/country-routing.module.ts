import {AddCountryComponent} from './add-country/add-country.component';
import {CountryComponent} from './country.component';
import {DetailCountryComponent} from './list-country/detail-country/detail-country.component';
import {EditCountryComponent} from './list-country/edit-country/edit-country.component';
import {ListCountryComponent} from './list-country/list-country.component';
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
  {
    path: 'country',
    component: CountryComponent,
    children: [
      {
        path: 'add',
        component: AddCountryComponent
      },
      {
        path: 'list',
        component: ListCountryComponent,
        children: [
          {
            path: 'view/:country-id',
            component: DetailCountryComponent
          },
          {
            path: 'edit/:country-id',
            component: EditCountryComponent
          },
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CountryRoutingModule {}
