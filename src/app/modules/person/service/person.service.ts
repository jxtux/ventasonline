import {Person} from '../../../domains/person';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {HttpParams} from '@angular/common/http';


@Injectable()
export class PersonService {
  private header = new Headers({'content-type': 'application/json'});

  constructor(protected httpClient: HttpClient) {}

  public get(): Observable<Person[]> {
    return this.httpClient.get<Person[]>('http://localhost/data/person.json');
  }

  getPerson(id: number): Observable<Person> {
    const httpParams = new HttpParams().set('id', id.toString());

    return this.httpClient.get<Person>('http://localhost/data/p.php', {params: httpParams});
  }
  
  updatePerson(person: Person): Observable<Person> {
    const httpParams = new HttpParams().set('person', JSON.stringify(person));

    return this.httpClient.get<Person>('http://localhost/data/update.php', {params: httpParams});
  }
}
