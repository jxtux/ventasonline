import {Person} from '../../../domains/person';
import {PersonService} from '../service/person.service';
import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-list-person',
  templateUrl: './list-person.component.html',
  styles: []
})
export class ListPersonComponent implements OnInit {

  private subscription: Subscription = new Subscription();

  persons: Person[];

  errorMsg: any;

  constructor(
    private personService: PersonService,
    private route: ActivatedRoute,
    private router: Router) {}

  ngOnInit() {
    this.getPersons();
  }

  getPersons() {

    const subscription = this.personService.get().subscribe(
      (data) => this.persons = data,
      (err) => this.errorMsg = err);

    this.subscription.add(subscription);
  }

  goToEdit(person: Person) {
    this.router.navigate([person.personId], {relativeTo: this.route});
  }

}
