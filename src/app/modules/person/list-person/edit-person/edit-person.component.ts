import {Person} from '../../../../domains/person';
import {PersonService} from '../../service/person.service';
import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router, ParamMap, Params} from '@angular/router';
import { Subscription } from 'rxjs';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-edit-person',
  templateUrl: './edit-person.component.html',
  styles: []
})
export class EditPersonComponent implements OnInit {
  person: Person;
  personForm: FormGroup;
  private subscription: Subscription = new Subscription();

  constructor(
    private personService: PersonService,
    private route: ActivatedRoute,
    private router: Router) {}

  ngOnInit() {
    this.personForm = new FormGroup({
      name: new FormControl(),
      city: new FormControl()
    });

    this.cargarParamData();
  }

  cargarParamData() {
    this.route.params
      .switchMap((params: Params) => this.personService.getPerson(+params['id']))
      .subscribe(person => {
        this.person = person;
        this.setFormValues();
      });
  }

  setFormValues() {
    this.personForm.setValue({name: this.person.name, city: this.person.city});
  }

  onFormSubmit() {
    this.person.name = this.personForm.get('name').value;
    this.person.city = this.personForm.get('city').value;

    const subscription = this.personService.updatePerson(this.person).subscribe(
      function (data:Person) {
        console.log('data de edit enviada:'+data.city);
        this.router.navigate(['../'], {relativeTo: this.route});
      },
      function (err) { this.errorMsg = err;});

    this.subscription.add(subscription);
  }
}