import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PersonRoutingModule} from './person-routing.module';
import {PersonComponent} from './person.component';
import {ListPersonComponent} from './list-person/list-person.component';
import {EditPersonComponent} from './list-person/edit-person/edit-person.component';
import {PersonService} from './service/person.service';
import { FormsModule, ReactiveFormsModule, } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    PersonRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [PersonComponent, ListPersonComponent, EditPersonComponent],
  providers: [PersonService]
})
export class PersonModule {
  constructor() { }

}
