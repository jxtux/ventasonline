import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { EditPersonComponent } from './list-person/edit-person/edit-person.component';
import { ListPersonComponent } from './list-person/list-person.component';
import { PersonComponent } from './person.component';

const routes: Routes = [
  {
    path: 'person',
    component: PersonComponent,
    children: [
      {
        path: '',
        component: ListPersonComponent,
        children: [
          {
            path: ':id',
            component: EditPersonComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonRoutingModule {}
